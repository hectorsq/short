defmodule ShortWeb.UrlController do
  use ShortWeb, :controller

  alias ShortWeb.Endpoint

  def show(conn, %{"id" => id}) do
    Endpoint.broadcast("click_topic", "short_link_clicked", %{short_link: id})

    conn
    |> put_resp_cookie("short", "This is my cookie")
    |> redirect(external: "https://google.com")
  end
end
