defmodule ShortWeb.ClickLive.Index do
  use ShortWeb, :live_view
  alias ShortWeb.Endpoint

  def mount(_params, _session, socket) do
    if connected?(socket) do
      Endpoint.subscribe("click_topic")
    end

    socket = assign(socket, clicks: [])
    {:ok, socket}
  end

  def handle_info(%{event: event, payload: payload} = _broadcast, socket) do
    IO.inspect(event)
    IO.inspect(payload)
    previous_clicks = socket.assigns[:clicks]
    updated_clicks = [payload | previous_clicks]
    socket = assign(socket, clicks: updated_clicks)
    {:noreply, socket}
  end
end
