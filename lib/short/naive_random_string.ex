defmodule Short.NaiveRandomString do
  @table "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
         |> String.codepoints()
         |> Enum.zip_reduce(Enum.to_list(0..61), %{}, fn x, y, acc ->
           Map.put(acc, y, x)
         end)

  def generate(length \\ 6) do
    length
    |> :crypto.strong_rand_bytes()
    |> :binary.bin_to_list()
    |> Enum.map(fn byte -> rem(byte, 62) end)
    |> Enum.map(fn index -> Map.get(@table, index) end)
    |> Enum.join()
  end
end
