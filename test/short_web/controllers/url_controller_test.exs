defmodule ShortWeb.UrlControllerTest do
  use ShortWeb.ConnCase

  describe "GET /1" do
    test "redirects to long URL", %{conn: conn} do
      conn = get(conn, "/1")
      assert "https://google.com" == redirected_to(conn)
    end

    test "gets the cookie in the response", %{conn: conn} do
      conn = get(conn, "/1")
      assert %{"short" => _} = conn.resp_cookies
    end
  end
end
