defmodule Short.NaiveRandomStringTest do
  use Short.DataCase, async: true

  alias Short.NaiveRandomString

  test "generates 6 alphanumeric characters by default" do
    string = NaiveRandomString.generate()
    assert Regex.match?(~r/^[[:alnum:]]{6}$/, string)
  end

  test "generates the required alphanumeric characters" do
    string = NaiveRandomString.generate(50)
    assert Regex.match?(~r/^[[:alnum:]]{50}$/, string)
  end
end
