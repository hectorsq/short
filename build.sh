#!/usr/bin/env bash

# exit on error
set -o errexit

# Creates a release
echo "Getting production dependencies..."
mix deps.get --only prod

echo "Compiling app in production mode..."
MIX_ENV=prod mix compile

echo "Compiling assets..."
MIX_ENV=prod mix assets.deploy

echo "Generating release..."
MIX_ENV=prod mix release --overwrite

echo "Release generated"
